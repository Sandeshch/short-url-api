package com.shorten.url.service;

public interface  UniqueKeyGenerationService {

	public abstract String generateUniqueKey();
	
}
